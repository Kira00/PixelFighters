This project was made as a student-project at the SAE Munich.

It was made in C++ with the Simple DirectMedia Layer(SDL) library.

It is a multiplayer beat 'em up where the player fight against each other in
an arena with multiple platforms. Each player gets 2 randomly assigned special-
attacks as well as a personalized finisher.
Additionally there are harmless slime-monster that can be killed to farm energy.

To play the game 2 pc-compatible controller are needed.